<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * ArticleRepository
 *
 */
class ArticleRepository extends EntityRepository
{
    /**
     *
     */
	public function findArticlesBySlug($slug)
	{

		return $this->createQueryBuilder('article')
			->andWhere('article.slug = :slug')
			->setParameter('slug', $slug)
			->getQuery()
			->getResult();
	}
	
}
