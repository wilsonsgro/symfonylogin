<?php

namespace AppBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ManagerArticle
{

    const FORMAT_JSON = 'json';
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function findAllArticles()
    {
        $em = $this->container->get("doctrine")->getManager();
        $articleRepo = $em->getRepository("AppBundle:Article");
        return $articleRepo->findAll();
    }

    public function findArticle($id)
    {
        $em = $this->container->get("doctrine")->getManager();
        $articleRepo = $em->getRepository("AppBundle:Article");
        return $articleRepo->find($id);
    }

    public function findAllArticlesToJason()
    {
        $articles = $this->findAllArticles();
        $serializer = $this->container->get('jms_serializer');
        return $serializer->serialize($articles, ManagerArticle::FORMAT_JSON);
    }

    public function findArticleToJason($id)
    {
        $article = $this->findArticle();
        $serializer = $this->container->get('jms_serializer');
        return $serializer->serialize($article, ManagerArticle::FORMAT_JSON);
    }
}
