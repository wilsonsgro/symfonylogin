<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    /**
     * @Route("/", name="home")
     *
     * @param Request $request
     *
     */
    public function indexAction(Request $request)
    {
        return $this->render('home.html.twig');
    }

    /**
     * @Route("/listarticles", name="detail_listarticles")
     */
    public function listArticlesAction(Request $request)
    {
        $managerArtcle = $this->get("app.manager");
        $articles = $managerArtcle->findAllArticles();
        
        return $this->render('article/articles.html.twig', ['articles' => $articles]);

    }

    /**
     * @Route("/article/{id}", name="detail_article")
     */
    public function detailArticleAction(Request $request, $id)
    {
        $managerArtcle = $this->get("app.manager");
        $article = $managerArtcle->findArticle($id);

        return $this->render('article/article.html.twig', ['article' => $article]);
    }
}
