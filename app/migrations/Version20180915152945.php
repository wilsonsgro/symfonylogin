<?php

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180915152945 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql("CREATE TABLE `user` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
          `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
          PRIMARY KEY (`id`),
          UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
          UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE `user`;");

    }
}
