<?php

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180915153527 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
               $this->addSql("CREATE TABLE `article` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

                $this->insert();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE `article`;");

    }

    private function insert()
    {
        $this->addSql("INSERT INTO `article` (`id`, `title`, `slug`, `image`, `description`)
VALUES
  (1, 'Lorem ipsum', 'Lorem-ipsum', 'https://newevolutiondesigns.com/images/freebies/galaxy-wallpaper-1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in porttitor nisi. Mauris elementum lorem massa, vitae rutrum enim posuere quis. Fusce vel felis porta, fermentum ante vitae, tincidunt augue. In hac habitasse platea dictumst. Etiam eget laoreet leo. Sed eu urna leo. Suspendisse ut quam ut leo malesuada dictum nec in odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis auctor euismod ex, ac aliquam nibh fringilla vitae. Nam vitae lacus et lectus maximus tincidunt. Integer feugiat tempus pharetra. Phasellus quis tellus dignissim, placerat metus id, suscipit velit.'),
  (2, 'Orlando furioso', 'orlando-furioso', 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Ariosto_-_Orlando_Furioso%2C_1551_-_5918999_FERE001606_00005.tif/lossy-page1-412px-Ariosto_-_Orlando_Furioso%2C_1551_-_5918999_FERE001606_00005.tif.jpg', 'Orlando furioso\r\nAriosto - Orlando Furioso, 1551 - 5918999 FERE001606 00005.tif\r\nFrontespizio dell\'edizione del 1551\r\nAutore Ludovico Ariosto\r\n1ª ed. originale  1516\r\nGenere  Poema\r\nSottogenere  Poema cavalleresco\r\nLingua originale  italiano\r\nProtagonisti  Orlando\r\nAltri personaggi Angelica, Ruggero, Bradamante, Agramante, Rinaldo, Medoro, Astolfo, ecc.\r\nL\'Orlando furioso è un poema cavalleresco di Ludovico Ariosto pubblicato per la prima volta nel 1516 a Ferrara.\r\n\r\nIl poema, composto da 46 canti in ottave (38.736 versi in totale), ruota attorno al personaggio di Orlando, a cui è dedicato il titolo, e a numerosi altri personaggi. L\'opera, riprendendo la tradizione del ciclo carolingio e in parte del ciclo bretone, si pone a continuazione dell\'incompiuto Orlando innamorato di Matteo Maria Boiardo: in seguito, tuttavia, Ariosto considererà l\'Orlando innamorato solo come una fonte a cui attingere, a causa della non attualità dei temi del poema, dovuti alla materia cavalleresca, ma riuscirà a risolvere questo problema apportando delle modificazioni interne all\'opera, tra cui l\'introduzione di tecniche narrative sconosciute al Boiardo, e soprattutto intervenendo spesso nel corso del poema spiegando al lettore il vero fine degli avvenimenti.'),
  (3, 'Ludovico Ariosto', 'ludovico-ariosto', 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Titian_-_Portrait_of_a_man_with_a_quilted_sleeve.jpg/440px-Titian_-_Portrait_of_a_man_with_a_quilted_sleeve.jpg', 'Ludovico Ariosto (Reggio nell\'Emilia, 8 settembre 1474 – Ferrara, 6 luglio 1533) è stato un poeta, commediografo, funzionario e diplomatico italiano, autore dell\'Orlando furioso (1516-1521-1532).\r\nStemma degli Ariosti\r\nÈ considerato uno degli autori più celebri ed influenti del suo tempo. Le sue opere, il Furioso in particolare, simboleggiano una potente rottura degli standard e dei canoni dell\'epoca. La sua ottava, definita \"ottava d\'oro\", rappresenta uno dei massimi della letteratura pre-illuminista.'),
  (4, 'Orlando innamorato', 'prlando -nnamorato', 'https://upload.wikimedia.org/wikipedia/commons/d/db/Orlando_innamorato.jpg', 'L\'Orlando innamorato (titolo originale L\'Inamoramento de Orlando) è un poema cavalleresco scritto da Matteo Maria Boiardo. Narra una successione di avventure fantastiche, duelli, amori e magie. Scritto in ottave (8 versi che rimano secondo lo schema ABABABCC), per permettere lo sviluppo di un discorso piuttosto lungo, è diviso in tre libri: il primo di ventinove canti, il secondo di trentuno e il terzo, appena iniziato, di otto canti e mezzo; ogni canto è costituito da una sessantina di ottave per un totale di 35.432 versi (divisi in 4.429 ottave).'),
  (5, 'Ottava rima', 'ottava-rima', 'https://images.slideplayer.com/20/5988440/slides/slide_36.jpg', 'L\'ottava rima è il metro usato nei cantari trecenteschi e nei poemetti del Boccaccio (Ninfale fiesolano, Filostrato,...); non è certo chi l\'abbia inventato, ma il suo uso può essere rintracciato fin dal XIV sec. (Britto di Bretagna di Antonio Pucci). Diventerà poi il metro di poeti popolari, come Antonio Pucci, e colti come Franco Sacchetti che lasceranno poi al Pulci, al Boiardo, all\'Ariosto e al Tasso, di elevarlo alle più alte cime. La popolarità dell\'ottava riuscì in questo modo a sostituire la terzina dantesca. È ancora questo metro che sarà utilizzato dai poeti estemporanei per i loro contrasti di improvvisazione fino ai nostri giorni.'),
  (6, 'Biblioteca nazionale Marciana', 'biblioteca-nazionale-marciana', 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Venice%2C_Libreria_Marciana.jpg/520px-Venice%2C_Libreria_Marciana.jpg', 'La Biblioteca nazionale Marciana (ovvero la biblioteca di San Marco) è una delle più grandi biblioteche italiane e la più importante di Venezia.\r\nContiene una delle più pregiate raccolte di manoscritti greci, latini ed orientali del mondo.\r\nNota anche come Biblioteca Marciana, Biblioteca di San Marco, Libreria Marciana, Libreria Sansoviniana, Libreria Vecchia o Libreria di San Marco, si trova sulla parte inferiore di Piazza San Marco, tra il Campanile di San Marco e la Zecca.'),
  (7, 'Orlando (paladino)', 'orlando-paladino', 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mort_de_Roland.jpg/520px-Mort_de_Roland.jpg', 'Orlando, Rolando o Hruodlandus (736 – Roncisvalle, 15 agosto 778) fu un prefetto della marca di Bretagna, assurto ad eroe nella Chanson de Roland.\r\nSecondo la Vita et gesta Caroli Magni di Eginardo, opera scritta tra l\'829 e l\'839, i Baschi massacrarono la retroguardia franca a Roncisvalle e uccisero Eggiardo, sovraintendente alla mensa del re, Anselmo, conte paladino, Orlando e molti altri (In quo proelio Eggihardus regiae mensae praepositus, Anshelmus come palatii et Hruodlandus Brittannici limiti praefectus cum aliis conpluribus interficiuntur).'),
  (8, 'Carlo Magno', 'carlo-magno', 'https://it.wikipedia.org/wiki/File:Louis-F%C3%A9lix_Amiel_-_Charlemagne_empereur_d%27Occident_(742-814).jpg', 'Carlo, detto Magno o Carlomagno (in tedesco Karl der Große, in francese Charlemagne, in latino Carolus Magnus; 2 aprile 742 – Aquisgrana, 28 gennaio 814), è stato re dei Franchi dal 768, re dei Longobardi dal 774 e dall\'800 primo imperatore del Sacro Romano Impero. L\'appellativo Magno (dal latino Magnus, \"grande\") gli fu dato dal suo biografo Eginardo, che intitolò la sua opera Vita et gesta Caroli Magni. Figlio di Pipino il Breve e Bertrada di Laon, Carlo divenne re nel 768, alla morte di suo padre. Regnò inizialmente insieme con il fratello Carlomanno, la cui improvvisa morte (avvenuta in circostanze misteriose nel 771) lasciò Carlo unico sovrano del regno franco. Grazie a una serie di fortunate campagne militari (compresa la conquista del Regno longobardo) allargò il regno dei Franchi fino a comprendere una vasta parte dell\'Europa occidentale.');");

    }
}
