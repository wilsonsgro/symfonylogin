Blacknachos
 -----------

### Requirements

![Docker >= 17.04 ](https://badgen.net/badge/Docker/>=17.04/409be6?icon=docker)

![docker-compose >=1.8.0 ](https://badgen.net/badge/docker-compose/>=1.8/409be6?icon=docker)
 
### Setup

```bash
cp .env.dist .env 
```

### Build

```bash
docker-compose up -d
```

### Composer install

```bash
docker-compose exec php composer install
```
if have problem with your memory RAM use this
```bash
docker-compose exec php php -d memory_limit=1 composer.phar install
```

#### Creation local development enviroment

```bash
docker-compose exec php bin/console --no-interaction doctrine:migrations:migrate --env=prod
docker-compose exec php bin/console cache:clear --env=prod
docker-compose exec php bin/console assets:install --symlink
```

#### Server name on your Mac/Linux (dns)

```bash
sudo nano /etc/hosts
127.0.0.1  local.blacknachos
```

#### Hey oh Let's Go

[local.blacknachos](http://local.blacknachos)


### Commands to enter containers

```bash
docker-compose exec php bash
docker-compose exec mariadb bash
docker-compose exec webserver bash
```

## Teach
[tutorial video](https://www.youtube.com/watch?v=wl3N5QJyeDM)
[How to Build a Traditional Login Form](https://symfony.com/doc/current/security/form_login_setup.html)
[How to Implement a Simple Registration Form](https://symfony.com/doc/current/doctrine/registration_form.html)


### Istall Symfony 3.4
```bash
composer create-project symfony/framework-standard-edition <name_your_project> "3.4"
```

###  After create new file php
```bash
docker-compose exec php composer dump-autoload
```
